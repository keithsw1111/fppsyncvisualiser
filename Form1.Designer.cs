﻿namespace FPPSyncVisualiser
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rejected = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.startTime = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lastSyncDelta = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.runningTime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lastSyncMedia = new System.Windows.Forms.TextBox();
            this.lastSyncFSEQ = new System.Windows.Forms.TextBox();
            this.lastSyncFrame = new System.Windows.Forms.TextBox();
            this.lastSyncPosition = new System.Windows.Forms.TextBox();
            this.lastSyncTimestamp = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.recentSync = new System.Windows.Forms.ListView();
            this.Timestamp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Type = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Position = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Delta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.FSEQ = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Media = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.errorMessage = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.errorMessage);
            this.groupBox1.Controls.Add(this.rejected);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.startTime);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lastSyncDelta);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.runningTime);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 148);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Current State";
            // 
            // rejected
            // 
            this.rejected.Location = new System.Drawing.Point(129, 115);
            this.rejected.Name = "rejected";
            this.rejected.ReadOnly = true;
            this.rejected.Size = new System.Drawing.Size(172, 22);
            this.rejected.TabIndex = 11;
            this.rejected.Text = "0";
            this.rejected.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "Rejected Packets:";
            // 
            // startTime
            // 
            this.startTime.Location = new System.Drawing.Point(129, 25);
            this.startTime.Name = "startTime";
            this.startTime.ReadOnly = true;
            this.startTime.Size = new System.Drawing.Size(172, 22);
            this.startTime.TabIndex = 9;
            this.startTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "Start Time:";
            // 
            // lastSyncDelta
            // 
            this.lastSyncDelta.Location = new System.Drawing.Point(129, 82);
            this.lastSyncDelta.Name = "lastSyncDelta";
            this.lastSyncDelta.ReadOnly = true;
            this.lastSyncDelta.Size = new System.Drawing.Size(172, 22);
            this.lastSyncDelta.TabIndex = 7;
            this.lastSyncDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Last Sync Delta:";
            // 
            // runningTime
            // 
            this.runningTime.Location = new System.Drawing.Point(129, 54);
            this.runningTime.Name = "runningTime";
            this.runningTime.ReadOnly = true;
            this.runningTime.Size = new System.Drawing.Size(172, 22);
            this.runningTime.TabIndex = 1;
            this.runningTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Time:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "FSEQ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Frame:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Position:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Media:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lastSyncMedia);
            this.groupBox2.Controls.Add(this.lastSyncFSEQ);
            this.groupBox2.Controls.Add(this.lastSyncFrame);
            this.groupBox2.Controls.Add(this.lastSyncPosition);
            this.groupBox2.Controls.Add(this.lastSyncTimestamp);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 166);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(776, 176);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Last Sync:";
            // 
            // lastSyncMedia
            // 
            this.lastSyncMedia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lastSyncMedia.Location = new System.Drawing.Point(113, 141);
            this.lastSyncMedia.Name = "lastSyncMedia";
            this.lastSyncMedia.ReadOnly = true;
            this.lastSyncMedia.Size = new System.Drawing.Size(657, 22);
            this.lastSyncMedia.TabIndex = 11;
            // 
            // lastSyncFSEQ
            // 
            this.lastSyncFSEQ.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lastSyncFSEQ.Location = new System.Drawing.Point(113, 113);
            this.lastSyncFSEQ.Name = "lastSyncFSEQ";
            this.lastSyncFSEQ.ReadOnly = true;
            this.lastSyncFSEQ.Size = new System.Drawing.Size(657, 22);
            this.lastSyncFSEQ.TabIndex = 10;
            // 
            // lastSyncFrame
            // 
            this.lastSyncFrame.Location = new System.Drawing.Point(113, 84);
            this.lastSyncFrame.Name = "lastSyncFrame";
            this.lastSyncFrame.ReadOnly = true;
            this.lastSyncFrame.Size = new System.Drawing.Size(172, 22);
            this.lastSyncFrame.TabIndex = 9;
            this.lastSyncFrame.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lastSyncPosition
            // 
            this.lastSyncPosition.Location = new System.Drawing.Point(113, 55);
            this.lastSyncPosition.Name = "lastSyncPosition";
            this.lastSyncPosition.ReadOnly = true;
            this.lastSyncPosition.Size = new System.Drawing.Size(172, 22);
            this.lastSyncPosition.TabIndex = 8;
            this.lastSyncPosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lastSyncTimestamp
            // 
            this.lastSyncTimestamp.Location = new System.Drawing.Point(113, 26);
            this.lastSyncTimestamp.Name = "lastSyncTimestamp";
            this.lastSyncTimestamp.ReadOnly = true;
            this.lastSyncTimestamp.Size = new System.Drawing.Size(172, 22);
            this.lastSyncTimestamp.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Timestamp:";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.recentSync);
            this.groupBox3.Location = new System.Drawing.Point(12, 348);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(776, 306);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Recent Sync Packets";
            // 
            // recentSync
            // 
            this.recentSync.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.recentSync.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Timestamp,
            this.Type,
            this.Position,
            this.Delta,
            this.FSEQ,
            this.Media});
            this.recentSync.HideSelection = false;
            this.recentSync.Location = new System.Drawing.Point(6, 21);
            this.recentSync.Name = "recentSync";
            this.recentSync.Size = new System.Drawing.Size(764, 279);
            this.recentSync.TabIndex = 0;
            this.recentSync.UseCompatibleStateImageBehavior = false;
            this.recentSync.View = System.Windows.Forms.View.Details;
            // 
            // Timestamp
            // 
            this.Timestamp.Text = "Timestamp";
            this.Timestamp.Width = 147;
            // 
            // Type
            // 
            this.Type.Text = "Type";
            // 
            // Position
            // 
            this.Position.Text = "Position";
            this.Position.Width = 115;
            // 
            // Delta
            // 
            this.Delta.Text = "Delta";
            this.Delta.Width = 132;
            // 
            // FSEQ
            // 
            this.FSEQ.Text = "FSEQ";
            this.FSEQ.Width = 159;
            // 
            // Media
            // 
            this.Media.Text = "Media";
            this.Media.Width = 142;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // errorMessage
            // 
            this.errorMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.errorMessage.Location = new System.Drawing.Point(307, 115);
            this.errorMessage.Name = "errorMessage";
            this.errorMessage.ReadOnly = true;
            this.errorMessage.Size = new System.Drawing.Size(463, 22);
            this.errorMessage.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 666);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "FPP Sync Visualiser";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox lastSyncDelta;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox runningTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox lastSyncMedia;
        private System.Windows.Forms.TextBox lastSyncFSEQ;
        private System.Windows.Forms.TextBox lastSyncFrame;
        private System.Windows.Forms.TextBox lastSyncPosition;
        private System.Windows.Forms.TextBox lastSyncTimestamp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView recentSync;
        private System.Windows.Forms.ColumnHeader Timestamp;
        private System.Windows.Forms.ColumnHeader Position;
        private System.Windows.Forms.ColumnHeader Delta;
        private System.Windows.Forms.ColumnHeader FSEQ;
        private System.Windows.Forms.ColumnHeader Media;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ColumnHeader Type;
        private System.Windows.Forms.TextBox startTime;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox rejected;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox errorMessage;
    }
}

