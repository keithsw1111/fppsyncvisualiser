﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace FPPSyncVisualiser
{
    public partial class Form1 : Form
    {
        FPPListener listener;
        List<SyncPacket> syncPackets = new List<SyncPacket>();
        Thread t;
        DateTime lastTimestamp;
        Mutex mutex = new Mutex();
        UInt32 error = 0;
        String message = "";

        public void AddPacket(SyncPacket pkt)
        {
            mutex.WaitOne();
            syncPackets.Add(pkt);
            mutex.ReleaseMutex();
        }

        public void ClearError()
        {
            mutex.WaitOne();
            message = "";
            mutex.ReleaseMutex();
        }

        public void AddError(String msg)
        {
            mutex.WaitOne();
            error++;
            message = msg;
            mutex.ReleaseMutex();
        }

        bool TrimPackets()
        {
            bool removed = false;

            // remove too many packets
            while (syncPackets.Count > 10)
            {
                syncPackets.RemoveAt(0);
                removed = true;
            }

            // remove really aged packets
            while (syncPackets.Count > 0 && (DateTime.Now - syncPackets.First().timestamp).TotalSeconds > 120)
            {
                syncPackets.RemoveAt(0);
                removed = true;
            }

            return removed;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listener = new FPPListener(this);
            t = new Thread(new ThreadStart(listener.Run));
            t.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            mutex.WaitOne();

            bool changed = TrimPackets();

            if (syncPackets.Count == 0 || syncPackets.Last().action == "Stop")
            {
                startTime.Text = "NOT RUNNING";
                runningTime.Text = "00:00:00.000";
                lastSyncDelta.Text = "00:00:00.000";
                lastSyncDelta.ForeColor = Color.Black;
                lastSyncFrame.Text = "";
                lastSyncFSEQ.Text = "";
                lastSyncMedia.Text = "";
                lastSyncPosition.Text = "";
            }
            else
            {
                runningTime.Text = listener.GetPosition().ToString(@"hh\:mm\:ss\.fff");
                SyncPacket last = syncPackets.Last();
                if (last.timestamp != lastTimestamp)
                {
                    changed = true;
                    lastTimestamp = last.timestamp;
                    startTime.Text = listener.GetStartTime().ToString(@"hh\:mm\:ss\.fff");
                    float delta = ((float)last.deltaMS / 1000);
                    lastSyncDelta.Text = delta.ToString();
                    if (delta > 0.100 || delta <-0.100)
                    {
                        lastSyncDelta.ForeColor = Color.Red;
                    }
                    else if (delta > 0.050 || delta < -0.050)
                    {
                        lastSyncDelta.ForeColor = Color.Orange;
                    }
                    else
                    {
                        lastSyncDelta.ForeColor = Color.Green;
                    }
                    lastSyncFrame.Text = last.frame.ToString();
                    if (last.IsFSEQ())
                    {
                        lastSyncFSEQ.Text = last.fseq;
                    }
                    if (last.IsMedia())
                    {
                        lastSyncMedia.Text = last.media;
                    }
                    lastSyncPosition.Text = new TimeSpan(last.positionMS * 10000).ToString(@"hh\:mm\:ss\.fff");
                }
            }

            rejected.Text = error.ToString();
            errorMessage.Text = message;

            if (changed)
            {
                recentSync.BeginUpdate();
                recentSync.Items.Clear();
                foreach (SyncPacket p in syncPackets)
                {
                    ListViewItem i = recentSync.Items.Add(p.timestamp.ToString());
                    i.SubItems.Add(p.action);
                    i.SubItems.Add(new TimeSpan(p.positionMS * 10000).ToString(@"hh\:mm\:ss\.fff"));
                    i.SubItems.Add(new TimeSpan(p.deltaMS * 10000).ToString(@"hh\:mm\:ss\.fff"));
                    i.SubItems.Add(p.fseq);
                    i.SubItems.Add(p.media);
                }
                if (syncPackets.Count > 0) recentSync.EnsureVisible(recentSync.Items.Count - 1);
                recentSync.EndUpdate();
            }

            mutex.ReleaseMutex();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            listener.Stop();
            t.Abort();
        }
    }

    public class SyncPacket
    {
        byte syncAction;
        byte syncType;
        public string action { get; set; }
        public string fseq { get; set; }
        public string media { get; set; }
        public DateTime timestamp { get; set; }
        public long positionMS { get; set; }
        public int frame { get; set; }
        public long deltaMS { get; set; }
        public bool IsStart() { return syncAction == 0x00; }
        public bool IsStop() { return syncAction == 0x01; }
        public bool IsSync() { return syncAction == 0x02; }
        public bool IsOpen() { return syncAction == 0x03; }
        public bool IsFSEQ() { return syncType == 0x00; }
        public bool IsMedia() { return syncType == 0x01; }

        public SyncPacket(byte[] packet, TimeSpan expectedPosition)
        {
            if (packet.Length < 7) throw new ApplicationException("Invalid FPP control packet");
            if (packet[0] != 'F' || packet[1] != 'P' || packet[2] != 'P' || packet[3] != 'D') throw new ApplicationException("Missing FPPD token");
            if (packet[4] != 0x01) throw new ApplicationException("Not a sync packet");
            int strlen = ((int)packet[6] << 8) + packet[5] - 10 - 1;

            if (packet.Length < 17 + strlen) 
                throw new ApplicationException("FPP sync packet too short");
            timestamp = DateTime.Now;
            syncAction = packet[7];
            switch(syncAction)
            {
                case 0x00: action = "Start"; break;
                case 0x01: action = "Stop"; break;
                case 0x02: action = "Sync"; break;
                case 0x03: action = "Open"; break;
                default: action = ""; break;
            }
            syncType = packet[8];
            frame = ((int)packet[12] << 24) + ((int)packet[11] << 16) + ((int)packet[10] << 8) + packet[9];

            // not sure how to extract the float
            positionMS = (long)(1000.0 * System.BitConverter.ToSingle(packet, 13));
            deltaMS = positionMS - (long)expectedPosition.TotalMilliseconds;

            for (int i = 0; i < strlen; i++)
            {
                if (IsFSEQ())
                {
                    fseq += (char)packet[17 + i];
                }
                else
                {
                    media += (char)packet[17 + i];
                }
            }
        }
    }

    class FPPListener
    {
        DateTime songStartTime;
        bool playing = false;
        Form1 form;
        bool stop = false;

        public DateTime GetStartTime() { return songStartTime; }

        public void Stop() { stop = true; }

        public bool IsPlaying() { return playing; }

        public FPPListener(Form1 f)
        {
            form = f;
        }

        public TimeSpan GetPosition()
        {
            if (playing)
            {
                return DateTime.Now - songStartTime;
            }
            else
            {
                return new TimeSpan(0);
            }
        }

        public void Run()
        {
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 32320);
            UdpClient newsock = new UdpClient(ipep);

            IPEndPoint ipmc = new IPEndPoint(IPAddress.Parse("239.70.80.80"), 32320);
            newsock.JoinMulticastGroup(ipmc.Address);

            byte[] message = new byte[1500];
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            while (!stop)
            {
                message = newsock.Receive(ref sender);
                try
                {
                    SyncPacket pkt = new SyncPacket(message, DateTime.Now - songStartTime);
                    songStartTime = DateTime.Now - new TimeSpan(pkt.positionMS * 10000);
                    form.AddPacket(pkt);
                    form.ClearError();

                    if (playing && pkt.action == "Stop") playing = false;
                    if (!playing && (pkt.action == "Start" || pkt.action == "Sync")) playing = true;
                }
                catch(Exception e)
                {
                    // we ignore these packets
                    form.AddError(e.Message);
                }
            }
            //log.Warn("Unreachable code ... reached.");
        }
    }
}
